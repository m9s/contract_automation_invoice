#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import datetime
import copy
from decimal import Decimal
from dateutil.relativedelta import relativedelta
from trytond.backend import TableHandler
from trytond.model import ModelView, ModelSQL, ModelWorkflow, fields
from trytond.pyson import PYSONEncoder, Equal, Eval, Not, In, Bool, If, Or, \
    And, Get
from trytond.transaction import Transaction
from trytond.modules.contract_automation.contract import _INTERTYPES
from trytond.pool import Pool


class Contract(ModelWorkflow, ModelSQL, ModelView):
    _name = 'contract.contract'

    invoice_address = fields.Many2One('party.address',
            'Contract Invoice Address',
            depends=[
                'has_invoice_automation', 'party_invoice', 'automation',
                'state',
            ], domain=[('party', If(Bool(Eval('party_invoice')), '=', '!='),
                    Eval('party_invoice'))],
            states={
                'required': And(Bool(Eval('has_invoice_automation')),
                        Bool(Eval('automation'))),
                'invisible': Or(Not(Bool(Eval('has_invoice_automation'))),
                        Not(Bool(Eval('automation')))),
                'readonly': Or(Not(Bool(Eval('party_invoice'))),
                        In(Eval('state'), ['cancel', 'terminated'])),
                }, help='If specified, the Contract Invoice Address overrides '
                'the Invoice Address of the party.',
            context={'party': Eval('party_invoice')})
    currency = fields.Many2One('currency.currency', 'Currency', required=True,
            states={
                'readonly': Not(Equal(Eval('state'), 'draft')),
                }, depends=['state'])
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_function_fields')
    payment_term = fields.Many2One('account.invoice.payment_term',
            'Payment Term', depends=[
                'has_invoice_automation', 'automation', 'state',
            ], states={
                'required': And(Bool(Eval('has_invoice_automation')),
                        Bool(Eval('automation'))),
                'invisible': Or(Not(Bool(Eval('has_invoice_automation'))),
                        Not(Bool(Eval('automation')))),
                'readonly': In(Eval('state'), ['cancel', 'terminated']),
                })
    lines = fields.One2Many('contract.line', 'contract',
            'Contract Lines', on_change=['lines', 'currency', 'party'],
            states={
                'readonly': Not(Equal(Eval('state'), 'draft')),
                'required': Bool(Eval('has_invoice_automation'))
                }, depends=['state', 'has_invoice_automation'])
    untaxed_amount = fields.Function(fields.Numeric('Untaxed',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']), 'get_function_fields')
    tax_amount = fields.Function(fields.Numeric('Tax',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']), 'get_function_fields')
    total_amount = fields.Function(fields.Numeric('Total',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']), 'get_function_fields')
    invoices = fields.Many2Many('contract.contract-account.invoice',
            'contract', 'invoice', 'Invoices', readonly=True)
    invoice_type = fields.Selection([
            ('out_invoice', 'Invoice'),
            ('in_invoice', 'Supplier Invoice'),
            ('out_credit_note', 'Credit Note'),
            ('in_credit_note', 'Supplier Credit Note'),
            ], 'Invoice Type', depends=[
                'has_invoice_automation', 'automation', 'state',
            ], states={
                'required': And(Bool(Eval('has_invoice_automation')),
                        Bool(Eval('automation'))),
                'invisible': Or(Not(Bool(Eval('has_invoice_automation'))),
                        Not(Bool(Eval('automation')))),
                'readonly': Not(Equal(Eval('state'), 'draft')),
                })
    has_invoice_automation = fields.Function(fields.Boolean(
            'Has Invoice Automation', on_change_with=['actions'],
            depends=['actions']), 'get_invoice_automation_fields')
    has_invoice_next_automation_date = fields.Function(fields.Boolean(
            'Has Invoice Next Automation Date', on_change_with=['actions'],
            depends=['actions', 'end_date']), 'get_invoice_automation_fields')
    party_invoice = fields.Function(fields.Many2One('party.party',
            'Party Invoice', depends=['parties'],
            on_change_with=['parties']), 'get_party_recipient',
            searcher='search_party_recipient')
    party_service = fields.Function(fields.Many2One('party.party',
            'Party Service', depends=['parties'],
            on_change_with=['parties']), 'get_party_recipient',
            searcher='search_party_recipient')

    def __init__(self):
        super(Contract, self).__init__()

        self._constraints += [
            ('check_end_date_invoiced', 'end_date_invoiced'),
            ('check_invoice_recipient', 'invoice_recipient')
        ]

        self._error_messages.update({
            'differing_contracts': 'The action with id(%s) is not an action '
                'of the given contract with id(%s)!',
            'missing_payment_term': 'Could not find payment term!\n'
                'Please set a payment term on party with code %s or '
                'on contract with code %s.',
            'end_date_invoiced': 'The end date of a contract must be after '
                'its last invoiced period!',
            'opened_paid_invoices': 'Contracts with invoices in state '
                '"Opened" or "Paid" can not be canceled!',
            'missing_time_of_supply_date': 'Missing date for time of supply!\n'
                '(Affected contract ID: %s)',
            'missing_fiscalyear for_date': 'Missing fiscal year for date %s!',
            'invoice_recipient': "Missing party of type 'Invoice Recipient'",
            })

        # Don't allow to change the end_date of contracts with automation, that
        # are already invoiced for the interval and due to termination
        state_end_date = And(
                Bool(Eval('automation')),
                Not(Bool(Eval('has_invoice_next_automation_date'))),
                Bool(Eval('invoices')))
        self.end_date = copy.copy(self.end_date)
        if self.end_date.states['readonly'] is None:
            self.end_date.states['readonly'] = state_end_date
        else:
            if PYSONEncoder().encode(state_end_date) not in \
                    PYSONEncoder().encode(self.end_date.states['readonly']):
                self.end_date.states['readonly'] = Or(
                        self.end_date.states['readonly'], state_end_date)
        if 'automation' not in self.end_date.depends:
            self.end_date.depends = copy.copy(self.end_date.depends)
            self.end_date.depends.append('automation')
        if 'has_invoice_next_automation_date' not in self.end_date.depends:
            self.end_date.depends = copy.copy(self.end_date.depends)
            self.end_date.depends.append('has_invoice_next_automation_date')
        if 'invoices' not in self.end_date.depends:
            self.end_date.depends = copy.copy(self.end_date.depends)
            self.end_date.depends.append('invoices')

        self._reset_columns()

    def default_invoice_type(self):
        return 'out_invoice'

    def default_company(self):
        return Transaction().context.get('company') or False

    def default_currency(self):
        company_obj = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        if company_id:
            company = company_obj.browse(company_id)
            return company.currency.id
        return False

    def default_currency_digits(self):
        company_obj = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        if company_id:
            company = company_obj.browse(company_id)
            return company.currency.digits
        return 2

    def default_payment_term(self):
        payment_term_obj = Pool().get('account.invoice.payment_term')
        payment_term_ids = payment_term_obj.search([])
        if len(payment_term_ids) == 1:
            return payment_term_ids[0]
        return False

    def on_change_party(self, vals):
        pool = Pool()
        party_obj = pool.get('party.party')
        address_obj = pool.get('party.address')
        payment_term_obj = pool.get('account.invoice.payment_term')
        res = {
            'invoice_address': False,
            'payment_term': False,
        }
        if vals.get('party'):
            party = party_obj.browse(vals['party'])
            res['invoice_address'] = party_obj.address_get(party.id,
                    type='invoice')
            if (vals.get('invoice_type') in ('in_invoice','in_credit_note')
                    and party.supplier_payment_term):
                res['payment_term'] = party.supplier_payment_term.id
            elif party.payment_term:
                res['payment_term'] = party.payment_term.id

        if res['invoice_address']:
            res['invoice_address.rec_name'] = address_obj.browse(
                    res['invoice_address']).rec_name
        if not res['payment_term']:
            res['payment_term'] = self.default_payment_term()
        if res['payment_term']:
            res['payment_term.rec_name'] = payment_term_obj.browse(
                    res['payment_term']).rec_name
        return res

    def on_change_lines(self, vals):
        pool = Pool()
        currency_obj = pool.get('currency.currency')
        tax_obj = pool.get('account.tax')
        invoice_obj = pool.get('account.invoice')

        res = {
            'untaxed_amount': Decimal('0.0'),
            'tax_amount': Decimal('0.0'),
            'total_amount': Decimal('0.0'),
        }

        currency = None
        if vals.get('currency'):
            currency = currency_obj.browse(vals['currency'])

        if vals.get('lines'):
            ctx = self.get_tax_context(vals)
            taxes = {}
            for line in vals['lines']:
                if line.get('type', 'line') != 'line':
                    continue
                res['untaxed_amount'] += line.get('amount', Decimal('0.0'))
                for tax in tax_obj.compute(line.get('taxes', []),
                        line.get('unit_price', Decimal('0.0')),
                        line.get('quantity', 0.0)):
                    with Transaction().set_context(**ctx):
                        key, val = invoice_obj._compute_tax(tax, 'out_invoice')
                    if not key in taxes:
                        taxes[key] = val['amount']
                    else:
                        taxes[key] += val['amount']
            if currency:
                for key in taxes:
                    res['tax_amount'] += currency_obj.round(currency,
                            taxes[key])
        if currency:
            res['untaxed_amount'] = currency_obj.round(currency,
                    res['untaxed_amount'])
            res['tax_amount'] = currency_obj.round(currency, res['tax_amount'])
        res['total_amount'] = res['untaxed_amount'] + res['tax_amount']
        if currency:
            res['total_amount'] = currency_obj.round(currency,
                    res['total_amount'])
        return res

    def on_change_with_has_invoice_automation(self, vals):
        return self._on_change_with_invoice_automation_fields(vals)

    def on_change_with_has_invoice_next_automation_date(self, vals):
        return self._on_change_with_invoice_automation_fields(vals)

    def _on_change_with_invoice_automation_fields(self, vals):
        action_type_obj = Pool().get('contract.action.type')
        if vals.get('actions'):
            type_ids = [x['type'] for x in vals['actions']]
            for type in action_type_obj.browse(type_ids):
                if type.function=='create_invoice_action':
                    return True
        return False

    def on_change_with_party_invoice(self, vals):
        return self._on_change_with_party_fields(vals, 'invoice_recipient')

    def on_change_with_party_service(self, vals):
        return self._on_change_with_party_fields(vals, 'service_recipient')

    def _on_change_with_party_fields(self, vals, recipient_type):
        party_type_obj = Pool().get('contract.party.type')

        res = False
        if vals.get('parties'):
            for values in vals['parties']:
                if values.get('type'):
                    type = party_type_obj.browse(values['type'])
                    if type.code == recipient_type:
                        res = values['party']
        return res

    def check_end_date_invoiced(self, ids):
        for contract in self.browse(ids):
            if contract.end_date:
                for action in contract.actions:
                    if action.next_invoicing_period_start:
                        if (contract.end_date
                                < action.next_invoicing_period_start):
                            return False
        return True

    def check_invoice_recipient(self, ids):
        party_type_obj = Pool().get('contract.party.type')
        type_id = party_type_obj.search([
                ('code', '=', 'invoice_recipient'),
                ])[0]
        for contract in self.browse(ids):
            res = False
            for party in contract.parties:
                if party.type.id == type_id:
                    res = True
                    break
            if not res:
                return False
        return True

    def get_function_fields(self, ids, names):
        '''
        Function to compute function fields for contract ids

        :param ids: the ids of the contracts
        :param names: the list of field name to compute
        :return: a dictionary with all field names as key and
            a dictionary as value with id as key
        '''
        res = {}
        contracts = self.browse(ids)
        if 'currency_digits' in names:
            res['currency_digits'] = self.get_currency_digits(contracts)
        if 'untaxed_amount' in names:
            res['untaxed_amount'] = self.get_untaxed_amount(contracts)
        if 'tax_amount' in names:
            res['tax_amount'] = self.get_tax_amount(contracts)
        if 'total_amount' in names:
            res['total_amount'] = self.get_total_amount(contracts)
        return res

    def get_untaxed_amount(self, contracts):
        '''
        Compute the untaxed amount for each contracts

        :param contracts: a BrowseRecordList of contracts
        :return: a dictionary with contract id as key and
            untaxed amount as value
        '''
        currency_obj = Pool().get('currency.currency')
        res = {}
        for contract in contracts:
            res.setdefault(contract.id, Decimal('0.0'))
            for line in contract.lines:
                if line.type != 'line':
                    continue
                res[contract.id] += line.amount
            if contract.currency:
                currency = contract.currency
            else:
                currency = contract.company.currency
            res[contract.id] = currency_obj.round(currency, res[contract.id])
        return res

    def get_tax_context(self, contract):
        party_obj = Pool().get('party.party')
        res = {}
        if isinstance(contract, dict):
            if contract.get('party_invoice'):
                party = party_obj.browse(contract['party_invoice'])
                if party.lang:
                    res['language'] = party.lang.code
        elif contract.party_invoice:
            if contract.party_invoice.lang:
                res['language'] = contract.party_invoice.lang.code
        else:
            if contract.company.lang:
                res['language'] = contract.company.lang.code
        return res

    def get_tax_amount(self, contracts):
        '''
        Compute the tax amount for contracts

        :param contracts: a BrowseRecordList of contracts
        :return: a dictionary with contract id as key and
            tax amount as value
        '''
        pool = Pool()
        currency_obj = pool.get('currency.currency')
        tax_obj = pool.get('account.tax')
        invoice_obj = pool.get('account.invoice')

        res = {}
        for contract in contracts:
            context = self.get_tax_context(contract)
            res.setdefault(contract.id, Decimal('0.0'))
            taxes = {}
            for line in contract.lines:
                if line.type != 'line':
                    continue
                # Don't round on each line to handle rounding error
                with Transaction().set_context(**context):
                    taxes_computed = tax_obj.compute(
                            [t.id for t in line.taxes], line.unit_price,
                            line.quantity)
                for tax in taxes_computed:
                    key, val = invoice_obj._compute_tax(tax, 'out_invoice')
                    if not key in taxes:
                        taxes[key] = val['amount']
                    else:
                        taxes[key] += val['amount']
            for key in taxes:
                res[contract.id] += currency_obj.round(contract.currency,
                        taxes[key])
            if contract.currency:
                currency = contract.currency
            else:
                currency = contract.company.currency
            res[contract.id] = currency_obj.round(currency, res[contract.id])
        return res

    def get_total_amount(self, contracts):
        '''
        Return the total amount of each contracts

        :param contracts: a BrowseRecordList of contracts
        :return: a dictionary with contract id as key and
            total amount as value
        '''
        currency_obj = Pool().get('currency.currency')
        res = {}
        untaxed_amounts = self.get_untaxed_amount(contracts)
        tax_amounts = self.get_tax_amount(contracts)
        for contract in contracts:
            if contract.currency:
                currency = contract.currency
            else:
                currency = contract.company.currency
            res[contract.id] = currency_obj.round(currency,
                    untaxed_amounts[contract.id] + tax_amounts[contract.id])
        return res

    def get_currency_digits(self, contracts):
        '''
        Return the number of digits of the currency of each contracts

        :param contracts: a BrowseRecordList of puchases
        :return: a dictionary with contract id as key and
            number of digits as value
        '''
        res = {}
        for contract in contracts:
            if contract.currency:
                res[contract.id] = contract.currency.digits
            else:
                res[contract.id] = 2
        return res

    def get_invoice_automation_fields(self, ids, names):
        '''
        Function to compute function fields for contract ids

        :param ids: the ids of the contracts
        :param names: the list of field name to compute
        :return: a dictionary with all field names as key and
            a dictionary as value with id as key
        '''
        res = {}
        contracts = self.browse(ids)
        if 'has_invoice_automation' in names:
            res['has_invoice_automation'] = \
                    self.get_has_invoice_automation(contracts)
        if 'has_invoice_next_automation_date' in names:
            res['has_invoice_next_automation_date'] = \
                    self.get_has_invoice_next_automation_date(contracts)
        return res

    def get_has_invoice_automation(self, contracts):
        res = {}

        for contract in contracts:
            res[contract.id] = False
            for action in contract.actions:
                if action.type.function=='create_invoice_action':
                    res[contract.id] = True
        return res

    def get_has_invoice_next_automation_date(self, contracts):
        res = {}

        for contract in contracts:
            res[contract.id] = False
            for action in contract.actions:
                if action.type.function=='create_invoice_action':
                    if action.next_automation_date:
                        res[contract.id] = True
        return res

    def get_party_recipient(self, ids, name):
        code = 'invoice_recipient'
        if name == "party_service":
            code = 'service_recipient'

        res = {}
        if not ids:
            return res

        contracts = self.browse(ids)
        for contract in contracts:
            res[contract.id] = False
            for party in contract.parties:
                if party.type and party.type.code==code:
                    res[contract.id] = party.party.id
        return res

    def search_party_recipient(self, name, clause):
        contract_party_obj = Pool().get('contract.party')
        contract_party_type_obj = Pool().get('contract.party.type')

        args = [('code', '=', 'invoice_recipient')]
        if name == "party_service":
            args = [('code', '=', 'service_recipient')]

        contract_party_type_ids = contract_party_type_obj.search(args)
        if not contract_party_type_ids:
            return [('id', 'in', [])]

        args = [
            ('party', clause[1], clause[2]),
            ('type', '=', contract_party_type_ids[0])
            ]
        contract_party_values = contract_party_obj.search_read(args,
                fields_names=['contract'])
        contract_ids = []
        for value in contract_party_values:
            contract_ids.append(value['contract'])
        return [('id', 'in', contract_ids)]

    def _get_invoice_line_contract_line(self, contract):
        '''
        Return invoice line values for contract lines

        :param contract: the BrowseRecord of the contract
        :return: a dictionary with invoice line as key
            and a list of invoice lines values as value
        '''
        line_obj = Pool().get('contract.line')
        res = {}
        for line in contract.lines:
            val = line_obj.get_invoice_line(line)
            if val:
                res[line.id] = val
        return res

    def _get_invoice_contract_action(self, action):
        res = self._get_invoice_contract(action.contract)
        res.update(self._get_accounting_dates(action))
        return res

    def _get_invoice_contract(self, contract):
        '''
        Return invoice values for contract

        :param contract: the BrowseRecord of the contract
        :return: a dictionary with invoice fields as key and
            invoice values as value
        '''
        party_obj = Pool().get('party.party')

        if contract.invoice_type in ('in_invoice', 'in_credit_note'):
            journal_type = 'expense'
            account = contract.party_invoice.account_payable.id
        else:
            journal_type = 'revenue'
            account = contract.party_invoice.account_receivable.id

        journal_obj = Pool().get('account.journal')

        journal_id = journal_obj.search([
            ('type', '=', journal_type),
            ], limit=1)
        if journal_id:
            journal_id = journal_id[0]

        invoice_address_id = (contract.invoice_address and
                contract.invoice_address.id) or False
        if not invoice_address_id:
            invoice_address_id = party_obj.address_get(
                contract.party_invoice.id, type='invoice')

        payment_term_id = (contract.payment_term and
                contract.payment_term.id) or False
        if not payment_term_id:
            payment_term_id = (contract.party_invoice.payment_term and
                    contract.party_invoice.payment_term.id) or False
            if not payment_term_id:
                self.raise_user_error('missing_payment_term', error_args=(
                        contract.party_invoice.code, contract.code))

        res = {
            'company': contract.company.id,
            'type': contract.invoice_type,
            'reference': contract.code,
            'journal': journal_id,
            'party': contract.party_invoice.id,
            'invoice_address': invoice_address_id,
            'currency': contract.currency.id,
            'account': account,
            'payment_term': payment_term_id,
            }
        return res

    def _get_accounting_dates(self, action):
        date_obj = Pool().get('ir.date')
        action_obj = Pool().get('contract.action')

        res = {}
        # time of supply must be calculated according to company timezone
        company_id = action.contract.company.id
        if action.next_invoicing_period_start:
            start_date = date_obj.local2companytime(company_id,
                    action.next_invoicing_period_start)
        else:
            start_date = date_obj.local2companytime(company_id,
                    action.contract.start_date)
        res['time_of_supply_start'] = start_date.date()

        new_start_date = action_obj.get_first_of_interval(start_date,
                action.automation_interval_unit)
        end_date = new_start_date + \
                _INTERTYPES[action.automation_interval_unit](
                action.automation_interval_number) - \
                relativedelta(days=1)

        if action.contract.end_date:
            contract_end_date = date_obj.local2companytime(company_id,
                    action.contract.end_date)
            if contract_end_date < end_date:
                end_date = contract_end_date
        end_date = end_date.date()
        res['time_of_supply_end'] = end_date
        res['accounting_date'] = end_date
        return res

    def create_invoice_action(self, action):
        res = self.create_invoice(action.contract, action=action)
        self.set_next_invoicing_period_start(action)
        return res

    def create_invoice(self, contract, action=False):
        '''
        Create the invoices for the contract

        :param contract: the BrowseRecord of the contract
        :param action: the BrowseRecord or id of the action
        :return: a list of the created invoice ids or None
        '''
        pool = Pool()
        action_obj = pool.get('contract.action')
        fiscalyear_obj = pool.get('account.fiscalyear')
        currency_obj = pool.get('currency.currency')

        if action:
            if isinstance(action, (int, long)):
                action = action_obj.browse(action)
            if action.contract.id != contract.id:
                self.raise_user_error('differing_contracts',
                        error_args=(action.id, contract.id))

        if not contract.party_invoice.account_receivable:
            self.raise_user_error('missing_account_receivable',
                    error_args=(contract.party_invoice.rec_name,))

        invoice_lines = self._get_invoice_line_contract_line(contract)
        if not invoice_lines:
            return

        if action:
            if contract.end_date and action.next_invoicing_period_start:
                if contract.end_date <= action.next_invoicing_period_start:
                    return
            vals = self._get_invoice_contract_action(action)
        else:
            vals = self._get_invoice_contract(contract)

        fiscalyear_ids = []
        # Check for time of supply crossing accounting periods
        supply_start = vals.get('time_of_supply_start')
        supply_end = vals.get('time_of_supply_end')
        if supply_start or supply_end:
            if not supply_start or not supply_end:
                self.raise_user_error('missing_time_of_supply_date',
                    error_args=(contract.id,))
            # TODO: change this when #1100 is solved
            fiscalyear_start_id = self.get_fiscalyear_id(supply_start)
            fiscalyear_end_id = self.get_fiscalyear_id(supply_end)
            if fiscalyear_start_id != fiscalyear_end_id:
                fiscalyear_ids = range(fiscalyear_start_id,
                    fiscalyear_end_id + 1)
            else:
                fiscalyear_ids = [fiscalyear_start_id]

        invoice_ids = []
        # Split in case of crossed accounting periods
        if len(fiscalyear_ids) > 1:
            # Compute percentage of days and accounting dates per fiscalyear
            fiscalyears = fiscalyear_obj.browse(fiscalyear_ids)
            days_per_year = {}
            total_days = supply_end - supply_start
            total_days = Decimal(total_days.days) + 1
            for fiscalyear in fiscalyears:
                days_per_year.setdefault(fiscalyear.id, {})
                start_date = supply_start
                if fiscalyear.start_date > supply_start:
                    start_date = fiscalyear.start_date
                end_date = supply_end
                if fiscalyear.end_date < supply_end:
                    end_date = fiscalyear.end_date
                diff_days = end_date - start_date
                diff_days = Decimal(diff_days.days) + 1
                days_per_year[fiscalyear.id]['days'] = diff_days
                days_per_year[fiscalyear.id]['start_date'] = start_date
                days_per_year[fiscalyear.id]['end_date'] = end_date
                days_per_year[fiscalyear.id]['percentage'] = \
                    diff_days / total_days
            # Apply percentage and dates to separate invoices and invoice lines
            sum_line_unit_price = {}
            count = 0
            steps = len(days_per_year) - 1
            last_key = 0
            for key, values in days_per_year.iteritems():
                sum_line_unit_price.setdefault(key, {})
                vals = copy.copy(vals)
                vals['time_of_supply_start'] = values['start_date']
                vals['time_of_supply_end'] = values['end_date']
                vals['accounting_date'] = values['end_date']
                vals['account'] = self.get_account_id_for_date(
                    vals['account'], vals['accounting_date'])
                invoice_lines2 = copy.deepcopy(invoice_lines)
                for id, value in invoice_lines2.iteritems():
                    if value[0].get('unit_price'):
                        # For the last invoice calculate the remainder instead
                        # of percentage to have correct sums
                        unit_price = invoice_lines[id][0]['unit_price']
                        if count < steps:
                            sum_line_unit_price[key].setdefault(
                                id, Decimal('0.0'))
                            unit_price *= values['percentage']
                            unit_price = currency_obj.round(contract.currency,
                                unit_price)
                            sum_line_unit_price[key][id] += unit_price
                            last_key = key
                        else:
                            unit_price -= sum_line_unit_price[last_key][id]
                        value[0]['unit_price'] = unit_price
                        value[0]['account'] = self.get_account_id_for_date(
                            value[0]['account'], vals['accounting_date'])
                invoice_id = self._create_invoice(contract, vals,
                    invoice_lines2)
                invoice_ids.append(invoice_id)
                count += 1
        else:
            invoice_ids = [self._create_invoice(contract, vals, invoice_lines)]

        return invoice_ids

    def _create_invoice(self, contract, vals, invoice_lines):
        '''
        Create an invoice from values

        :param contract: the BrowseRecord of the contract
        :param vals: a dict of vals for the invoice
        :param invoice_lines: a dictionary with invoice line as key
                        and a list of invoice lines values as value
        :return: the id of the invoice or None
        '''
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        invoice_line_obj = pool.get('account.invoice.line')
        contract_line_obj = pool.get('contract.line')

        with Transaction().set_user(0):
            with Transaction().set_context(company=contract.company.id):
                invoice_id = invoice_obj.create(vals)

        for line_id in invoice_lines:
            for line_vals in invoice_lines[line_id]:
                line_vals['invoice'] = invoice_id
                with Transaction().set_user(0):
                    with Transaction().set_context(
                            company=contract.company.id):
                        invoice_line_id = invoice_line_obj.create(line_vals)

                contract_line_obj.write(line_id, {
                    'invoice_lines': [('add', invoice_line_id)],
                    })

        with Transaction().set_user(0):
            with Transaction().set_context(company=contract.company.id):
                invoice_obj.update_taxes([invoice_id])

        self.write(contract.id, {
            'invoices': [('add', invoice_id)],
        })

        return invoice_id

    def get_fiscalyear_id(self, date):
        # monkey patch from account_timeline
        # TODO: remove this when #1100 is solved
        fiscalyear_obj = Pool().get('account.fiscalyear')

        res = False
        args = [
            ('start_date', '<=', date),
            ('end_date', '>=', date)
            ]
        fiscalyear_ids = fiscalyear_obj.search(args)
        if not fiscalyear_ids:
            self.raise_user_error('missing_fiscalyear_for_date',
                    error_args=(date,))
        res = fiscalyear_ids[0]
        return res

    def set_next_invoicing_period_start(self, action):
        action_obj = Pool().get('contract.action')
        date_obj = Pool().get('ir.date')

        # intervals must be computed for company timezone
        if action.next_invoicing_period_start:
            start_date = date_obj.local2companytime(action.contract.company.id,
                action.next_invoicing_period_start)
        else:
            start_date = date_obj.local2companytime(action.contract.company.id,
                action.contract.start_date)
            start_date = action_obj.get_first_of_interval(start_date,
                    action.automation_interval_unit)

        new_start_date = start_date + \
                _INTERTYPES[action.automation_interval_unit](
                    action.automation_interval_number)
        new_start_date = date_obj.company2localtime(action.contract.company.id,
                new_start_date)

        if action.contract.end_date:
            if new_start_date > action.contract.end_date:
                new_start_date = False
        vals = {
            'next_invoicing_period_start': new_start_date,
            }
        res = action_obj.write(action.id, vals)
        return res

    def terminate_contract(self, action):
        # enable automatic termination of contracts for ended contracts
        if action.contract.end_date and action.next_invoicing_period_start:
            if action.contract.end_date <= action.next_invoicing_period_start:
                vals = {
                    'next_automation_date': False,
                    }
                self.write(action.id, vals)
        return super(Contract, self).terminate_contract(action)

    def check_cancel(self, contract_id):
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        invoice_line_obj = pool.get('account.invoice.line')
        contract_line_obj = pool.get('contract.line')

        res = super(Contract, self).check_cancel(contract_id)
        contract = self.browse(contract_id)
        invoices = contract.invoices
        invoice_ids = [x.id for x in invoices if x.state in ('open', 'paid')]
        if invoice_ids:
            self.raise_user_error('opened_paid_invoices')
        else:
            # cancel related invoices
            for invoice in invoices:
                invoice_obj.workflow_trigger_validate(invoice.id, 'cancel')
            # unlink all related invoice lines and invoices
            contract_line_ids = contract_line_obj.search([
                    ('contract', '=', contract_id),
                    ])
            contract_lines = contract_line_obj.browse(contract_line_ids)
            for line in contract_lines:
                contract_line_obj.write(line.id, {
                    'invoice_lines': [('unlink_all',)]
                    })
                invoice_line_obj.delete([x.id for x in line.invoice_lines])
            self.write(contract_id, {
                    'invoices': [('unlink_all',)]
                    })
            # delete related invoices
            invoice_obj.delete([x.id for x in invoices])

        return res

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['invoices'] = False
        return super(Contract, self).copy(ids, default=default)

    def get_account_id_for_date(self, account_id, date):
        '''
        Get the account_id for a given date.
        Method to override in dependent modules.

        :param account_id: the id of the account
        :param date: the date of the account
        :return: the id of the invoice line account
        '''
        return account_id

Contract()


class ContractInvoice(ModelSQL):
    'Contract - Invoice'
    _name = 'contract.contract-account.invoice'
    _description = __doc__
    contract = fields.Many2One('contract.contract', 'Contract',
            ondelete='RESTRICT', select=1, required=True)
    invoice = fields.Many2One('account.invoice', 'Invoice',
            ondelete='CASCADE', select=1, required=True)

ContractInvoice()


class ContractLine(ModelSQL, ModelView):
    'Contract Line'
    _name = 'contract.line'
    _description = __doc__
    _rec_name = 'description'

    contract = fields.Many2One('contract.contract', 'Contract',
            required=True, ondelete='CASCADE')
    sequence = fields.Integer('Sequence')
    type = fields.Selection([
        ('line', 'Line'),
        ('subtotal', 'Subtotal'),
        ('title', 'Title'),
        ('comment', 'Comment'),
        ], 'Type', select=1, required=True)
    quantity = fields.Float('Quantity',
            digits=(16, Eval('unit_digits', 2)),
            states={
                'invisible': Not(Equal(Eval('type'), 'line')),
                'required': Equal(Eval('type'), 'line'),
            }, depends=['unit_digits', 'type'])
    unit = fields.Many2One('product.uom', 'Unit',
            states={
                'required': Bool(Eval('product')),
                'invisible': Not(Equal(Eval('type'), 'line')),
            }, domain=[('category', '=',
                    (Eval('product'), 'product.default_uom.category'))],
            context={
                'category': (Eval('product'), 'product.default_uom.category')
            }, depends=['product', 'type'],
            on_change=['product', 'quantity', 'unit',
                '_parent_contract.currency',
                '_parent_contract.party'])
    unit_digits = fields.Function(fields.Integer('Unit Digits',
            on_change_with=['unit']), 'get_unit_digits')
    product = fields.Many2One('product.product', 'Product',
            states={
                'invisible': Not(Equal(Eval('type'), 'line')),
            }, on_change=['product', 'unit', 'quantity', 'description',
                '_parent_contract.party', '_parent_contract.currency',
                '_parent_contract.invoice_type'], depends=['type'])
    unit_price = fields.Numeric('Unit Price', digits=(16, 4),
            states={
                'invisible': Not(Equal(Eval('type'), 'line')),
                'required': Equal(Eval('type'), 'line'),
            }, depends=['type'])
    amount = fields.Function(fields.Numeric('Amount',
            digits=(16,
                Get(Eval('_parent_contract', {}), 'currency_digits', 2)),
            states={
                'invisible': Not(In(Eval('type'), ['line', 'subtotal'])),
            }, on_change_with=['type', 'quantity', 'unit_price',
                '_parent_contract.currency'], depends=['type']), 'get_amount')
    description = fields.Text('Description', size=None, required=True)
    note = fields.Text('Note')
    taxes = fields.Many2Many('contract.line-account.tax',
            'line', 'tax', 'Taxes', domain=[('parent', '=', False)],
            states={
                'invisible': Not(Equal(Eval('type'), 'line')),
            }, depends=['type'])
    invoice_lines = fields.Many2Many('contract.line-account.invoice.line',
            'contract_line', 'invoice_line', 'Invoice Lines', readonly=True)

    def __init__(self):
        super(ContractLine, self).__init__()
        self._error_messages.update({
            'missing_account_revenue': 'Missing '
                    '"Account Revenue" on product "%s"!',
            'missing_account_expense': 'Missing '
                    '"Account Expense" on product "%s"!',
            'missing_account_revenue_property': 'Missing '
                    '"Account Revenue" default property!',
            'missing_account_expense_property': 'Missing '
                    '"Account Expense" default property!',
            })
        self._order.insert(0, ('sequence', 'ASC'))

    def default_type(self):
        return 'line'

    def get_unit_digits(self, ids, name):
        res = {}
        for line in self.browse(ids):
            if line.unit:
                res[line.id] = line.unit.digits
            else:
                res[line.id] = 2
        return res

    def on_change_with_unit_digits(self, vals):
        uom_obj = Pool().get('product.uom')
        if vals.get('unit'):
            uom = uom_obj.browse(vals['unit'])
            return uom.digits
        return 2

    def _get_tax_rule_pattern(self, party, vals):
        '''
        Get tax rule pattern

        :param party: the BrowseRecord of the party
        :param vals: a dictionary with value from on_change
        :return: a dictionary to use as pattern for tax rule
        '''
        res = {}
        return res

    def on_change_product(self, vals):
        pool = Pool()
        product_obj = pool.get('product.product')
        party_obj = pool.get('party.party')
        company_obj = pool.get('company.company')
        currency_obj = pool.get('currency.currency')
        tax_rule_obj = pool.get('account.tax.rule')

        if not vals.get('product'):
            return {}
        res = {}

        party = None
        party_lang_code = 'en_US'
        if vals.get('_parent_contract.party'):
            party = party_obj.browse(vals.get('_parent_contract.party'))
            if party.lang:
                party_lang_code = party.lang.code

        product = product_obj.browse(vals['product'])

        company = None
        company_id = Transaction().context.get('company') or False
        if company_id:
            company = company_obj.browse(company_id)
        currency = None
        if vals.get('_parent_contract.currency'):
            #TODO check if today date is correct
            currency = currency_obj.browse(
                    vals.get('_parent_contract.currency'))

        #Braucht es einen Contract Type? out_invoice, out_credit_note?
        if vals.get('_parent_contract.invoice_type') \
                in ('in_invoice', 'in_credit_note'):
            if company and currency:
                res['unit_price'] = currency_obj.compute(company.currency,
                        product.cost_price, currency, round=False)
            else:
                res['unit_price'] = product.cost_price

            res['taxes'] = []
            pattern = self._get_tax_rule_pattern(party, vals)
            for tax in product.supplier_taxes_used:
                if party and party.supplier_tax_rule:
                    tax_ids = tax_rule_obj.apply(
                            party.supplier_tax_rule, tax, pattern)
                    if tax_ids:
                        res['taxes'].extend(tax_ids)
                    continue
                res['taxes'].append(tax.id)
            if party and party.supplier_tax_rule:
                tax_ids = tax_rule_obj.apply(party.supplier_tax_rule, False,
                        pattern)
                if tax_ids:
                    res['taxes'].extend(tax_ids)
        else:
            if company and currency:
                res['unit_price'] = currency_obj.compute(company.currency,
                        product.list_price, currency, round=False)
            else:
                res['unit_price'] = product.list_price

            res['taxes'] = []
            pattern = self._get_tax_rule_pattern(party, vals)
            for tax in product.customer_taxes_used:
                if party and party.customer_tax_rule:
                    tax_ids = tax_rule_obj.apply(party.customer_tax_rule, tax,
                            pattern)
                    if tax_ids:
                        res['taxes'].extend(tax_ids)
                    continue
                res['taxes'].append(tax.id)
            if party and party.customer_tax_rule:
                tax_ids = tax_rule_obj.apply(party.customer_tax_rule, False,
                        pattern)
                if tax_ids:
                    res['taxes'].extend(tax_ids)

        if not vals.get('description'):
            with Transaction().set_context(language=party_lang_code):
                res['description'] = product_obj.browse(product.id).rec_name

        category = product.default_uom.category
        if not vals.get('unit') \
                or vals.get('unit') not in [x.id for x in category.uoms]:
            res['unit'] = product.default_uom.id
            res['unit.rec_name'] = product.default_uom.rec_name
            res['unit_digits'] = product.default_uom.digits

        vals = vals.copy()
        vals['unit_price'] = res['unit_price']
        vals['type'] = 'line'
        res['amount'] = self.on_change_with_amount(vals)
        return res

    def on_change_with_amount(self, vals):
        currency_obj = Pool().get('currency.currency')
        if vals.get('type') == 'line':
            if isinstance(vals.get('_parent_contract.currency'), (int, long)):
                currency = currency_obj.browse(
                        vals['_parent_contract.currency'])
            else:
                currency = vals['_parent_contract.currency']
            amount = Decimal(str(vals.get('quantity') or '0.0')) * \
                    (vals.get('unit_price') or Decimal('0.0'))
            if currency:
                return currency_obj.round(currency, amount)
            return amount
        return Decimal('0.0')

    def on_change_unit(self, vals):
        return {}

    def get_amount(self, ids, name):
        currency_obj = Pool().get('currency.currency')
        res = {}
        for line in self.browse(ids):
            if line.type == 'line':
                res[line.id] = currency_obj.round(line.contract.currency,
                        Decimal(str(line.quantity)) * line.unit_price)
            elif line.type == 'subtotal':
                res[line.id] = Decimal('0.0')
                for line2 in line.contract.lines:
                    if line2.type == 'line':
                        res[line.id] += currency_obj.round(
                                line2.contract.currency,
                                Decimal(str(line2.quantity))
                                * line2.unit_price)
                    elif line2.type == 'subtotal':
                        if line.id == line2.id:
                            break
                        res[line.id] = Decimal('0.0')
            else:
                res[line.id] = Decimal('0.0')
        return res

    def get_invoice_line(self, line):
        '''
        Return invoice line values for contract line

        :param line: the BrowseRecord of the line

        :return: a list of invoice line values
        '''
        res = {}
        res['sequence'] = line.sequence
        res['type'] = line.type
        res['description'] = line.description
        res['note'] = line.note

        #without this line an access error is raised!
        res['company'] = line.contract.company.id

        if line.type != 'line':
            return [res]

        res['quantity'] = line.quantity
        res['unit'] = line.unit and line.unit.id or False
        res['product'] = line.product and line.product.id or False
        res['unit_price'] = line.unit_price
        res['taxes'] = [('set', [x.id for x in line.taxes])]
        res['account'] = self.get_invoice_line_account(line)
        return [res]

    def get_invoice_line_account(self, line):
        '''
        Return the account value of an invoice line for a contract line

        :param line: the BrowseRecord of the contract line
        :return: the id of the invoice line account
        '''
        property_obj = Pool().get('ir.property')

        account_id = None
        if line.contract.invoice_type in ('in_invoice', 'in_credit_note'):
            account_type = 'expense'
        else:
            account_type = 'revenue'

        if line.product:
            account_id = line.product['account_' + account_type + '_used'].id
            if not account_id:
                self.raise_user_error('missing_account_' + account_type,
                        error_args=(line.product.rec_name,))
        else:
            for model in ('product.template', 'product.category'):
                account_id = property_obj.get('account_' + account_type,
                        model)
                if account_id:
                    break
            if not account_id:
                self.raise_user_error('missing_account_' + account_type +
                        '_property')
        return account_id

ContractLine()


class ContractLineAccountTax(ModelSQL, ModelView):
    'Contract Line Account Tax'
    _name = 'contract.line-account.tax'
    _description = __doc__
    _rec_name = 'contract'

    line = fields.Many2One('contract.line', 'Contract Line', required=True,
            ondelete='CASCADE')
    tax = fields.Many2One('account.tax', 'Tax', required=True,
            ondelete='RESTRICT')

ContractLineAccountTax()


class ContractLineInvoiceLine(ModelSQL, ModelView):
    'Contract Line Invoice Line'
    _name = 'contract.line-account.invoice.line'
    _description = __doc__
    _rec_name = 'contract_line'

    contract_line = fields.Many2One('contract.line', 'Contract Line',
            required=True, ondelete='CASCADE')
    invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line',
            required=True, ondelete='RESTRICT')

ContractLineInvoiceLine()


class ContractAction(ModelSQL, ModelView):
    _name = 'contract.action'

    next_invoicing_period_start = fields.DateTime(
        'Next Invoicing Period Start', readonly=True)

    def init(self, module_name):
        date_obj = Pool().get('ir.date')
        user_obj = Pool().get('res.user')
        cursor = Transaction().cursor
        table = TableHandler(cursor, self, module_name)

        # Migration of next_invoicing_period_start form date to datetime
        migrate = False
        column = 'next_invoicing_period_start'
        if table.column_exist(column):
            sql_all = 'SELECT id, "' + column + '" FROM "' + self._table +'"' \
                    'WHERE "' + column + '" IS NOT NULL'
            sql_limit = sql_all + ' LIMIT 1'
            cursor.execute(sql_limit)
            with Transaction().set_user(0):
                for _, act_date in cursor.fetchall():
                    if not isinstance(act_date, datetime.datetime):
                        migrate = True
        super(ContractAction, self).init(module_name)
        if migrate:
            # Use timezone of cron user, whose timezone was used before
            # company_timezone (which will be created in the same run as
            # this migration)
            (user_id, ) = user_obj.search([
                ('name', '=', 'Cron Process Contract Action')
                ])
            cursor.execute(sql_all)
            with Transaction().set_user(0):
                for id, old_date in cursor.fetchall():
                    new_date = date_obj.user2localtime(user_id, old_date)
                    new_date = new_date.isoformat(' ')
                    cursor.execute('UPDATE "' + self._table + '" ' \
                            'SET "' + column + '" = %s ' \
                            'WHERE id = %s', (new_date, id))

    def default_type(self):
        action_type_obj = Pool().get('contract.action.type')
        args = [('function', '=', 'create_invoice_action')]
        action_ids = action_type_obj.search(args, limit=1)
        if action_ids:
            return action_ids[0]
        return False

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['next_invoicing_period_start'] = False
        return super(ContractAction, self).copy(ids, default=default)

    def get_actions_to_process(self):
        now = datetime.datetime.now()
        args = ['AND', ['OR', ('next_automation_date', '<', now),
                ['AND', ('next_automation_date', '=', False),
                ('contract.start_date', '<=', now),
                ('contract.invoices', '=', False)]],
                ('contract.automation', '=', True),
                ('contract.state', '=', 'active')]
        action_ids = self.search(args)
        return action_ids

ContractAction()
