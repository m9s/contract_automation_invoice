# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Contract Automation Invoice',
    'name_de_DE': 'Vertrag Automatik Fakturierung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Contract Automation with Invoicing
    - Adds invoicing to automatic contract management
''',
    'description_de_DE': '''Fakturierung für die Vertragsautomatik
    - Fügt die Fakturierung als Automatikfunktion zur Vertragsverwaltung hinzu
''',
    'depends': [
        'account_invoice_time_supply',
        'contract_automation',
    ],
    'xml': [
        'contract.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
